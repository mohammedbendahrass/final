window.addEventListener('scroll', function() {
  var header = document.querySelector('header');
  header.classList.toggle('background-header', window.scrollY > 350);
});
